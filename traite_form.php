<?php
session_start();
$message = '';
$echec = false;

// Tester la validité des champs
if (isset($_REQUEST['visiteur'])) {
    $visiteur = $_REQUEST['visiteur'];
    $_SESSION['visiteur'] = $visiteur;
} else {
    $message .= "*** le champ visiteur doit être renseigné ! ***<br/>";
    $echec = true;
}
if (isset($_REQUEST['phrase'])) {
    $phrase = $_REQUEST['phrase'];
} else {
    $message .= "*** le champ phrase doit être renseigné ! ***<br/>";
    $echec = true;
}

$strRep = "
        <p> %s </p>
        <h3>Merci, %s !</h3>
        <h4>La phrase que vous m'avez fournie était : %s</h4>\n";
$strErr = "<h3><font color='red'> %s </font></h3>\n";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mon formulaire</title>
    </head>
    <body>
<?php
if (!$echec) {
    echo sprintf($strRep, $visiteur, $visiteur, $phrase);
} elseif (isset($_SESSION['visiteur'])) {
    echo "Bonjour ".$_SESSION['visiteur'];
} else {
    echo sprintf($strErr, $message);
}
?>
    </body>
</html>
